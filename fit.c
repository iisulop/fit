
#include <stdlib.h>
#include <string.h>

#include "fit.h"
#include "fit_parser.h"
#include "fit_utils.h"

const static size_t MAX_BUF = 512;

fit_t*
fit_loadb(const uint8_t* buf, const size_t len, const size_t flags, fit_error_t* error)
{
  fit_t* data = NULL;

  *error = FIT_ERROR_GENERAL;

  data = (fit_t*)calloc(1, sizeof(fit_t));
  if (data == NULL)
    {
      FIT_LOG(FIT_LOG_ERROR, "Out of memory.\n");
      *error = FIT_ERROR_OUT_OF_MEMORY;
      return NULL;
    }

  if (buf == 0 || len <= 0)
    {
      FIT_LOG(FIT_LOG_ERROR, "Out of memory.\n");
      free(data);
      *error = FIT_ERROR_OUT_OF_MEMORY;
      return NULL;
    }

  data->blob.data = (uint8_t*)malloc(len);
  if (data->blob.data == NULL)
    {
      FIT_LOG(FIT_LOG_ERROR, "Out of memory.\n");
      free(data);
      *error = FIT_ERROR_OUT_OF_MEMORY;
      return NULL;
    }

  memcpy(data->blob.data, buf, len);
  data->blob.size = len;
  data->blob.pos = 0;

  *error = fit_parse_header(data);
  if (*error != FIT_SUCCESS)
    {
      FIT_LOG(FIT_LOG_ERROR, "Parsing header failed.\n");
      free(data);
      free(data->blob.data);
      return NULL;
    }

  return data;
}

void
fit_release(fit_t** d)
{
  fit_t* data = *d;
  fit_definition_message_t* definition = NULL;
  fit_definition_message_t* tmp_def = NULL;
  fit_field_definition_t* field = NULL;
  fit_field_definition_t* tmp_field = NULL;

  if (data != NULL)
    {
      if (data->blob.data != NULL)
        {
          free(data->blob.data);
          data->blob.data = NULL;
        }
      if (data->definitions != NULL)
        {
          definition = data->definitions;
          while (definition != NULL)
            {
              field = definition->fields;
              while (field != NULL)
                {
                  tmp_field = field;
                  field = (fit_field_definition_t*)field->next;
                  free(tmp_field);
                  tmp_field = NULL;
                }

              tmp_def = definition;
              definition = (fit_definition_message_t*)definition->next;
              free(tmp_def);
              tmp_def = NULL;
            }
        }
      free(data);
      data = NULL;
    }
}

#ifndef NO_FILE_SUPPORT
fit_t*
fit_loadf(const char* filename, const size_t flags, fit_error_t* error)
{
  fit_t* data = NULL;
  FILE* fp = NULL;
  size_t len = 0;
  uint8_t* buf = NULL;

  *error = FIT_ERROR_GENERAL;

  if (filename == NULL)
    {
      FIT_LOG(FIT_LOG_ERROR, "No filename.\n");
      *error = FIT_ERROR_CANNOT_OPEN_FILE;
      return NULL;
    }

  fp = fopen(filename, "r");

  if(fp != NULL)
    {
      if(fseek(fp, 0L, SEEK_END) != 0)
        {
          FIT_LOG(FIT_LOG_ERROR, "Cannot get file length.\n");
          *error = FIT_ERROR_CANNOT_GET_FILE_LENGTH;
          goto out;
        }

      len = ftell( fp );
      if(len == (size_t)-1)
        {
          FIT_LOG(FIT_LOG_ERROR, "Cannot get file length\n");
          *error = FIT_ERROR_CANNOT_GET_FILE_LENGTH;
          goto out;
        }

      buf = malloc(len);
      if (buf == NULL)
        {
          FIT_LOG(FIT_LOG_ERROR, "Out of memory.\n");
          *error = FIT_ERROR_OUT_OF_MEMORY;
          goto out;
        }

      if(fseek(fp, 0L, SEEK_SET) != 0 || fread(buf, 1, len, fp) == 0)
        {
          FIT_LOG(FIT_LOG_ERROR, "Cannot read file.\n");
          *error = FIT_ERROR_CANNOT_READ_FILE;
          free(buf);
          goto out;
        }

      data = fit_loadb(buf, len, flags, error);
      free(buf);
      if (*error != FIT_SUCCESS)
        {
          goto out;
        }
      buf = NULL;

      *error = FIT_SUCCESS;
    }
  else
    {
      FIT_LOG(FIT_LOG_ERROR, "Cannot open file %s.\n", filename);
      *error = FIT_ERROR_CANNOT_OPEN_FILE;
    }

out:
  if (fp != NULL)
    {
      fclose(fp);
    }

  return data;
}
#endif /*NO_FILE_SUPPORT*/

fit_message_t*
fit_get_next_data_message(fit_t* fit, fit_error_t* error)
{
  fit_message_t* message = NULL;
  *error = FIT_ERROR_GENERAL;
  if (fit != NULL)
    {
      do
        {
          *error = fit_parse_record(fit, &message);
        } while (*error == FIT_SUCCESS && message == NULL);
    }
  else
    {
      *error = FIT_ERROR_DATA_NULL;
    }

  return message;
}

fit_data_field_t*
fit_get_next_data_field(fit_message_t* message, fit_data_field_t* field)
{
  fit_data_field_t* next = NULL;
  if (field != NULL)
    {
      next = (fit_data_field_t*)field->next;
      free(field);
    }
  else if (message != NULL)
    {
      next = message->fields;
    }

  return next;
}

uint8_t
fit_is_enum(fit_data_field_t* field)
{
  uint8_t ret_val = 0;
  if (field == NULL)
    {
      return ret_val;
    }

  switch (field->type)
    {
    case ENUM:
      ret_val = 1;
      break;
    default:
      ret_val = 0;
    }
  return ret_val;
}

uint8_t
fit_is_number(fit_data_field_t* field)
{
  uint8_t ret_val = 0;
  if (field == NULL)
    {
      return ret_val;
    }

  switch (field->type)
    {
    case ENUM:
    case S8:
    case U8:
    case S16:
    case U16:
    case S32:
    case U32:
    case FLOAT32:
    case FLOAT64:
    case U8Z:
    case U16Z:
    case U32Z:
    case BYTE:
      ret_val = 1;
      break;
    default:
      ret_val = 0;
    }
  return ret_val;
}

uint8_t
fit_is_integer(fit_data_field_t* field)
{
  uint8_t ret_val = 0;
  if (field == NULL)
    {
      return ret_val;
    }

  switch (field->type)
    {
    case ENUM:
    case S8:
    case U8:
    case S16:
    case U16:
    case S32:
    case U32:
    case U8Z:
    case U16Z:
    case U32Z:
    case BYTE:
      ret_val = 1;
      break;
    default:
      ret_val = 0;
    }
  return ret_val;
}


uint8_t
fit_is_signed(fit_data_field_t* field)
{
  uint8_t ret_val = 0;
  if (field == NULL)
    {
      return ret_val;
    }

  switch (field->type)
    {
    case S8:
    case S16:
    case S32:
    case FLOAT32:
    case FLOAT64:
      ret_val = 1;
      break;
    default:
      ret_val = 0;
    }
  return ret_val;
}

uint8_t
fit_is_unsigned(fit_data_field_t* field)
{
  uint8_t ret_val = 0;
  if (field == NULL)
    {
      return ret_val;
    }

  switch (field->type)
    {
    case U8:
    case U16:
    case U32:
    case U8Z:
    case U16Z:
    case U32Z:
    case BYTE:
      ret_val = 1;
      break;
    default:
      ret_val = 0;
    }
  return ret_val;
}

uint8_t
fit_is_real(fit_data_field_t* field)
{
  uint8_t ret_val = 0;
  if (field == NULL)
    {
      return ret_val;
    }

  switch (field->type)
    {
    case FLOAT32:
    case FLOAT64:
      ret_val = 1;
      break;
    default:
      ret_val = 0;
    }
  return ret_val;
}

uint8_t
fit_is_string(fit_data_field_t* field)
{
  uint8_t ret_val = 0;
  if (field == NULL)
    {
      return ret_val;
    }

  switch (field->type)
    {
    case STRING:
      ret_val = 1;
      break;
    default:
      ret_val = 0;
    }
  return ret_val;
}

fit_error_t
fit_get_enum_value(fit_data_field_t* field, uint8_t* value)
{
  fit_error_t error = FIT_ERROR_GENERAL;
  if (field == NULL || value == NULL)
    {
      error = FIT_ERROR_DATA_NULL;
      goto out;
    }

  switch (field->type)
    {
    case ENUM:
      *value = field->data.enm;
      error = FIT_SUCCESS;
      break;
    default:
      error = FIT_ERROR_NOT_ENUM;
      break;
    }

out:
  return error;
}

fit_error_t
fit_get_integer_value(fit_data_field_t* field, int64_t* value)
{
  fit_error_t error = FIT_ERROR_GENERAL;
  if (field == NULL || value == NULL)
    {
      error = FIT_ERROR_DATA_NULL;
      goto out;
    }

  switch (field->type)
    {
    case S8:
      *value = field->data.s8;
      error = FIT_SUCCESS;
      break;
    case U8:
      *value = field->data.u8;
      error = FIT_SUCCESS;
      break;
    case S16:
      *value = field->data.s16;
      error = FIT_SUCCESS;
      break;
    case U16:
      *value = field->data.u16;
      error = FIT_SUCCESS;
      break;
    case S32:
      *value = field->data.s32;
      error = FIT_SUCCESS;
      break;
    case U32:
      *value = field->data.u32;
      error = FIT_SUCCESS;
      break;
    case U8Z:
      *value = field->data.u8z;
      error = FIT_SUCCESS;
      break;
    case U16Z:
      *value = field->data.u16z;
      error = FIT_SUCCESS;
      break;
    case U32Z:
      *value = field->data.u32z;
      error = FIT_SUCCESS;
      break;
    case BYTE:
      *value = field->data.byte;
      error = FIT_SUCCESS;
      break;
    default:
      error = FIT_ERROR_NOT_ENUM;
      break;
    }

out:
  return error;
}

fit_error_t
fit_get_signed_value(fit_data_field_t* field, int64_t* value)
{
  fit_error_t error = FIT_ERROR_GENERAL;
  if (field == NULL || value == NULL)
    {
      error = FIT_ERROR_DATA_NULL;
      goto out;
    }

  switch (field->type)
    {
    case S8:
      *value = field->data.s8;
      error = FIT_SUCCESS;
      break;
    case S16:
      *value = field->data.s16;
      error = FIT_SUCCESS;
      break;
    case S32:
      *value = field->data.s32;
      error = FIT_SUCCESS;
      break;
    default:
      error = FIT_ERROR_NOT_ENUM;
      break;
    }

out:
  return error;
}

fit_error_t
fit_get_unsigned_value(fit_data_field_t* field, uint64_t* value)
{
  fit_error_t error = FIT_ERROR_GENERAL;
  if (field == NULL || value == NULL)
    {
      error = FIT_ERROR_DATA_NULL;
      goto out;
    }

  switch (field->type)
    {
    case U8:
      *value = field->data.u8;
      error = FIT_SUCCESS;
      break;
    case U16:
      *value = field->data.u16;
      error = FIT_SUCCESS;
      break;
    case U32:
      *value = field->data.u32;
      error = FIT_SUCCESS;
      break;
    case U8Z:
      *value = field->data.u8z;
      error = FIT_SUCCESS;
      break;
    case U16Z:
      *value = field->data.u16z;
      error = FIT_SUCCESS;
      break;
    case U32Z:
      *value = field->data.u32z;
      error = FIT_SUCCESS;
      break;
    case BYTE:
      *value = field->data.byte;
      error = FIT_SUCCESS;
      break;
    default:
      error = FIT_ERROR_NOT_ENUM;
      break;
    }

out:
  return error;
}

fit_error_t
fit_get_double_value(fit_data_field_t* field, double* value)
{
  fit_error_t error = FIT_ERROR_GENERAL;
  if (field == NULL || value == NULL)
    {
      error = FIT_ERROR_DATA_NULL;
      goto out;
    }

  switch (field->type)
    {
    case FLOAT32:
      *value = field->data.float32;
      error = FIT_SUCCESS;
      break;
    case FLOAT64:
      *value = field->data.float64;
      error = FIT_SUCCESS;
      break;
    default:
      error = FIT_ERROR_NOT_ENUM;
      break;
    }

out:
  return error;
}

fit_error_t
fit_get_string_value(fit_data_field_t* field, char** value)
{
  fit_error_t error = FIT_ERROR_GENERAL;
  if (field == NULL || value == NULL)
    {
      error = FIT_ERROR_DATA_NULL;
      goto out;
    }

  switch (field->type)
    {
    case STRING:
      *value = malloc(strnlen(field->data.string, MAX_BUF));
      if (*value == NULL)
        {
          error = FIT_ERROR_OUT_OF_MEMORY;
          goto out;
        }
      strncpy(*value, field->data.string, MAX_BUF);
      error = FIT_SUCCESS;
      break;
    default:
      error = FIT_ERROR_NOT_ENUM;
      break;
    }

out:
  return error;
}

void
fit_release_message(fit_message_t* message)
{
}


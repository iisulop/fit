#ifndef FIT_UTILS_H
#define FIT_UTILS_H

#include <stdint.h>

#include "fit_types.h"

enum fit_debug_level_t
{
  FIT_LOG_DEBUG2,
  FIT_LOG_DEBUG,
  FIT_LOG_VERBOSE,
  FIT_LOG_INFO,
  FIT_LOG_ERROR
};

#define fit_defined_debug_level FIT_LOG_DEBUG
#define LOG_OUTPUT stdout
#define FIT_LOG( fit_debug_level, ...) do{ \
    if( fit_debug_level >= fit_defined_debug_level ) \
      fprintf(LOG_OUTPUT, __VA_ARGS__); \
    }while(0)

uint8_t
get8bits_from_blob( uint8_t* blob );

uint8_t
get8bits( fit_binary_t* blob );

uint16_t
get16bits_from_blob( uint8_t* blob, uint8_t architecture );

uint16_t
get16bits( fit_binary_t* blob, uint8_t endianness );

uint32_t
get32bits_from_blob( uint8_t* blob, uint8_t architecture );

uint32_t
get32bits( fit_binary_t* blob, uint8_t endianness );

#endif /* FIT_UTILS_H */

#include <stdlib.h>
#include <string.h>

#include "fit_base_type_parsers.h"

#include "fit_types.h"
#include "fit_utils.h"


void* fit_parse_base_type_enum(uint8_t* blob, uint8_t* size,
                                uint8_t architecture)
{
  void* ret_val = NULL;
  uint8_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing enum: ");
  if(*size == 1)
    {
      value = get8bits_from_blob(blob);
      ret_val = malloc(*size);
      *(uint8_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%02X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_s8(uint8_t* blob, uint8_t* size,
                              uint8_t architecture)
{
  void* ret_val = NULL;
  int8_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing s8: ");
  if(*size == 1)
    {
      value = get8bits_from_blob(blob);
      ret_val = malloc(*size);
      *(int8_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%02X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_u8(uint8_t* blob, uint8_t* size,
                              uint8_t architecture)
{
  void* ret_val = NULL;
  uint8_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing u8: ");
  if(*size == 1)
    {
      value = get8bits_from_blob(blob);
      ret_val = malloc(*size);
      *(uint8_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%02X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_s16(uint8_t* blob, uint8_t* size,
                               uint8_t architecture)
{
  void* ret_val = NULL;
  int16_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing s16: ");
  if(*size == 2)
    {
      value = get16bits_from_blob(blob, architecture);
      ret_val = malloc(*size);
      *(int16_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%04X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_u16(uint8_t* blob, uint8_t* size,
                               uint8_t architecture)
{
  void* ret_val = NULL;
  uint16_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing u16: ");
  if(*size == 2)
    {
      value = get16bits_from_blob(blob, architecture);
      ret_val = malloc(*size);
      *(uint16_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%04X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_s32(uint8_t* blob, uint8_t* size,
                               uint8_t architecture)
{
  void* ret_val = NULL;
  int32_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing s32: ");
  if(*size == 4)
    {
      value = get32bits_from_blob(blob, architecture);
      ret_val = malloc(*size);
      *(int32_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%08X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_u32(uint8_t* blob, uint8_t* size,
                               uint8_t architecture)
{
  void* ret_val = NULL;
  uint32_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing u32: ");
  if(*size == 4)
    {
      value = get32bits_from_blob(blob, architecture);
      ret_val = malloc(*size);
      *(uint32_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%08X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_string(uint8_t* blob, uint8_t* size,
                                  uint8_t architecture)
{
  void* ret_val = NULL;
  uint8_t i = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing string: ");
  *size = strlen((char*)blob);
  ret_val = malloc(*size);
  memcpy(ret_val, blob, *size);
  for(i = 0; i < *size; ++i)
    {
      FIT_LOG(FIT_LOG_DEBUG2, "%02x ", ((uint8_t*)(ret_val))[i]);
    }
  FIT_LOG(FIT_LOG_DEBUG2, "\n");
  return ret_val;
}

void* fit_parse_base_type_float32(uint8_t* blob, uint8_t* size,
                                   uint8_t architecture)
{
  void* ret_val = NULL;
  float value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing float32: ");
  if(*size == 4)
    {
      value = get32bits_from_blob(blob, architecture);
      ret_val = malloc(*size);
      *(float*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%08X / %f\n", (uint32_t)value, value);
  return ret_val;
}

void* fit_parse_base_type_float64(uint8_t* blob, uint8_t* size,
                                   uint8_t architecture)
{
  void* ret_val = NULL;
  double value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing float64: ");
  if(*size == 8)
    {
      value = get32bits_from_blob(blob, architecture);
      ret_val = malloc(*size);
      *(double*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%16X / %f\n", (uint32_t)value, value);
  return ret_val;
}

void* fit_parse_base_type_u8z(uint8_t* blob, uint8_t* size,
                               uint8_t architecture)
{
  void* ret_val = NULL;
  uint8_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing u8z: ");
  if(*size == 1)
    {
      value = get8bits_from_blob(blob);
      ret_val = malloc(*size);
      *(uint8_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%02X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_u16z(uint8_t* blob, uint8_t* size,
                                uint8_t architecture)
{
  void* ret_val = NULL;
  uint16_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing u16z: ");
  if(*size == 2)
    {
      value = get16bits_from_blob(blob, architecture);
      ret_val = malloc(*size);
      *(uint16_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%04X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_u32z(uint8_t* blob, uint8_t* size,
                                uint8_t architecture)
{
  void* ret_val = NULL;
  uint32_t value = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing u32z: ");
  if(*size == 4)
    {
      value = get32bits_from_blob(blob, architecture);
      ret_val = malloc(*size);
      *(uint32_t*)ret_val = value;
    }
  FIT_LOG(FIT_LOG_DEBUG2, "%02X / %d\n", value, value);
  return ret_val;
}

void* fit_parse_base_type_byte(uint8_t* blob, uint8_t* size,
                               uint8_t architecture)
{
  void* ret_val = NULL;
  uint8_t i = 0;
  FIT_LOG(FIT_LOG_DEBUG2, "Parsing bytes: ");
  ret_val = malloc(*size);
  memcpy(ret_val, blob, *size);
  for(i = 0; i < *size; ++i)
    {
      FIT_LOG(FIT_LOG_DEBUG2, "%02x ", ((uint8_t*)(ret_val))[i]);
    }
  FIT_LOG(FIT_LOG_DEBUG2, "\n");
  return ret_val;
}


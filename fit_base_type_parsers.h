#ifndef FIT_BASE_TYPE_PARSERS_H
#define FIT_BASE_TYPE_PARSERS_H

#include <stdint.h>

void* fit_parse_base_type_enum( uint8_t* blob, uint8_t* size,
                                uint8_t architecture );

void* fit_parse_base_type_s8( uint8_t* blob, uint8_t* size,
                              uint8_t architecture );

void* fit_parse_base_type_u8( uint8_t* blob, uint8_t* size,
                              uint8_t architecture );

void* fit_parse_base_type_s16( uint8_t* blob, uint8_t* size,
                               uint8_t architecture );

void* fit_parse_base_type_u16( uint8_t* blob, uint8_t* size,
                               uint8_t architecture );

void* fit_parse_base_type_s32( uint8_t* blob, uint8_t* size,
                               uint8_t architecture );

void* fit_parse_base_type_u32( uint8_t* blob, uint8_t* size,
                               uint8_t architecture );

void* fit_parse_base_type_string( uint8_t* blob, uint8_t* size,
                                  uint8_t architecture );

void* fit_parse_base_type_float32( uint8_t* blob, uint8_t* size,
                                   uint8_t architecture );

void* fit_parse_base_type_float64( uint8_t* blob, uint8_t* size,
                                   uint8_t architecture );

void* fit_parse_base_type_u8z( uint8_t* blob, uint8_t* size,
                               uint8_t architecture );

void* fit_parse_base_type_u16z( uint8_t* blob, uint8_t* size,
                                uint8_t architecture );

void* fit_parse_base_type_u32z( uint8_t* blob, uint8_t* size,
                                uint8_t architecture );

void* fit_parse_base_type_byte( uint8_t* blob, uint8_t* size,
                                uint8_t architecture );

#endif

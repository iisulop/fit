CC=gcc

CFLAGS+=-Wno-variadic-macros -g -Wall -pedantic -ansi
LDFLAGS+=-g

parse: parse.o fit.o fit_base_type_parsers.o fit_utils.o fit_parser.o
	$(CC) $(LDFLAGS) $^ -o $@

parse.o: fit.h

fit.o: fit.c fit.h

fit_base_type_parsers.o: fit_base_type_parsers.c fit_base_type_parsers.h

fit_utils.o: fit_utils.c fit_utils.h

fit_parser.o: fit_parser.h fit_parser.c



clean:
	-rm *.o parse

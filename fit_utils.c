#include "fit_utils.h"

uint8_t
get8bits_from_blob( uint8_t *blob )
{
  uint8_t ret_val = (*(uint8_t*)(blob));
  /*
     FIT_LOG(FIT_LOG_DEBUG, "GOT1 %0X\n", ret_val);
  */
  return ret_val;
}

uint8_t
get8bits( fit_binary_t* blob)
{
  uint8_t ret_val = get8bits_from_blob( &(blob->data[blob->pos]) );
  blob->pos += 1;
  return ret_val;
}

uint16_t
get16bits_from_blob( uint8_t *blob, uint8_t architecture )
{
  uint16_t ret_val = (*(uint16_t*)(blob));
  /*
     FIT_LOG(FIT_LOG_DEBUG, "GOT2 %d\n", ret_val);
   */
  if( architecture )
    {
      uint8_t tmp = (uint8_t)((ret_val & 0xFF00) >> 8);
      ret_val <<= 8;
      ret_val += tmp;
    }

  return ret_val;
}

uint16_t
get16bits( fit_binary_t* blob, uint8_t architecture )
{
  uint16_t ret_val =
    get16bits_from_blob( &(blob->data[blob->pos]), architecture );
  blob->pos += 2;
  return ret_val;
}

uint32_t
get32bits_from_blob( uint8_t *blob, uint8_t architecture )
{
  uint32_t ret_val = (*(uint32_t*)(blob));
  /*
     FIT_LOG(FIT_LOG_DEBUG, "GOT4 %d\n", ret_val);
  */
  if( architecture )
    {
      uint8_t tmp = (uint8_t)((ret_val & 0xFF000000) >> 24);
      ret_val &= 0x00FFFFFF;
      ret_val += (tmp << 24);
      tmp = (uint8_t)((ret_val & 0x00FF0000) >> 16);
      ret_val &= 0xFF00FFFF;
      ret_val += (tmp << 16);
    }
  return ret_val;
}

uint32_t
get32bits( fit_binary_t* blob, uint8_t architecture )
{
  uint32_t ret_val =
    get32bits_from_blob( &(blob->data[blob->pos]), architecture );
  blob->pos += 4;
  return ret_val;
}


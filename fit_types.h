#ifndef FIT_TYPES_H
#define FIT_TYPES_H

#include <stdint.h>
#include <stdio.h>

#include "fit_base_type_parsers.h"

typedef enum
{
  ENUM = 0,
  S8,
  U8,
  S16,
  U16,
  S32,
  U32,
  STRING,
  FLOAT32,
  FLOAT64,
  U8Z,
  U16Z,
  U32Z,
  BYTE
} fit_type_name_t;

typedef struct
{
  uint8_t base_type;
  uint8_t endian_ability;
  uint8_t base_type_field;
  fit_type_name_t type;
  uint64_t invalid_value;
  uint8_t size;
  void* (*parse_type)( uint8_t* blob, uint8_t* size, uint8_t architecture );
} fit_base_type_t;

static const fit_base_type_t FIT_BASE_TYPES[] = {
  {0, 0, 0x00, ENUM, 0xFF, 1, &fit_parse_base_type_enum},
  {1, 0, 0x01, S8, 0x7F, 1, &fit_parse_base_type_s8},
  {2, 0, 0x02, U8, 0xFF, 1, &fit_parse_base_type_u8},
  {3, 1, 0x83, S16, 0x7FFF, 2, &fit_parse_base_type_s16},
  {4, 1, 0x84, U16, 0xFFFF, 2, &fit_parse_base_type_u16},
  {5, 1, 0x85, S32, 0x7FFFFF, 4, &fit_parse_base_type_s32},
  {6, 1, 0x86, U32, 0xFFFFFF, 4, &fit_parse_base_type_u32},
  {7, 0, 0x07, STRING, 0x00, 1, &fit_parse_base_type_string}, /* NULL terminated string */
  {8, 1, 0x88, FLOAT32, 0xFFFFFFFF, 4, &fit_parse_base_type_float32},
  {9, 1, 0x89, FLOAT64, 0xFFFFFFFF, 8, &fit_parse_base_type_float64},
  {10, 0, 0x0A, U8Z, 0x00, 1, &fit_parse_base_type_u8z},
  {11, 1, 0x8B, U16Z, 0x0000, 2, &fit_parse_base_type_u16z},
  {12, 1, 0x8C, U32Z, 0x000000, 4, &fit_parse_base_type_u32z},
  {13, 0, 0x0D, BYTE, 0xFF, 1, &fit_parse_base_type_byte}
};

typedef enum
{
  FIT_FILE_DEVICE = 1,
  FIT_FILE_SETTINGS = 2,
  FIT_FILE_SPORT_SETTINGS = 3,
  FIT_FILE_ACTIVITY = 4,
  FIT_FILE_WORKOUT = 5,
  FIT_FILE_COURSE = 6,
  FIT_FILE_SCHEDULE = 7,
  FIT_FILE_WEIGHT = 9,
  FIT_FILE_TOTALS = 10,
  FIT_FILE_GOALS = 11,
  FIT_FILE_BLOOD_PRESSURE = 14,
  FIT_FILE_MONITORING = 15,
  FIT_FILE_ACTIVITY_SUMMARY = 20,
  FIT_FILE_DAILY_MONITORING = 21
} fit_file_type_t;

#define FIT_BASE_TYPE_ENDIAN_ABILITY_BIT 0x80
#define FIT_BASE_TYPE_NUMBER_MASK 0x0F

#define FIT_COMPRESSED_TIMESTAMP_HEADER_BIT 0x80
#define FIT_COMPRESSED_TIMESTAMP_HEADER_LOCAL_MESSAGE_TYPE_MASK 0x60
#define FIT_COMPRESSED_TIMESTAMP_HEADER_TIME_OFFSET_MASK 0x0F

#define FIT_NORMAL_HEADER_MASK 0xF0
#define FIT_DEFINITION_MESSAGE_BIT 0x40
#define FIT_NORMAL_HEADER_LOCAL_MESSAGE_TYPE_MASK 0x0F

typedef struct
{
  uint8_t header_size;
  uint8_t protocol_version;
  uint16_t profile_version;
  uint32_t data_size;
  char ascii_dotfit[4];
  uint16_t crc;
} fit_file_header_t;

struct fit_field_definition_t;
typedef struct
{
  uint8_t field_definition_number;
  uint8_t size;
  uint8_t base_type;
  struct fit_field_definition_t* next;
} fit_field_definition_t;

#define FIT_ARCHITECTURE_BIG_ENDIAN 0x01
struct fit_definition_message_t;
typedef struct
{
  uint8_t local_message_type;
  uint8_t reserved;
  uint8_t architecture;
  uint16_t global_message_number;
  uint8_t num_fields;
  fit_field_definition_t* fields;
  struct fit_definition_message_t* next;
} fit_definition_message_t;

typedef struct
{
  uint8_t* data;
  size_t size;
  size_t pos;
} fit_binary_t;

typedef struct
{
  fit_file_header_t header;
  fit_binary_t blob;
  fit_definition_message_t* definitions;
} fit_t;

struct fit_data_field_t;
typedef struct
{
  fit_type_name_t type;
  uint8_t field_num;
  union
    {
      uint8_t enm;
      int8_t s8;
      uint8_t u8;
      int16_t s16;
      uint16_t u16;
      int32_t s32;
      uint32_t u32;
      char* string;
      float float32;
      /* TODO: This might not be correct */
      double float64;
      uint8_t u8z;
      uint16_t u16z;
      uint32_t u32z;
      uint8_t byte;
  } data;
  struct fit_data_field_t* next;
} fit_data_field_t;

struct fit_message_t;
typedef struct
{
  uint16_t global_message_number;
  fit_data_field_t* fields;
  struct fit_message_t* next;
} fit_message_t;

#endif

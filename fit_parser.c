
#include <string.h>

#include "fit_parser.h"
#include "fit_utils.h"

static const char DOTFIT[] = ".FIT";
static const uint8_t HEADER_SIZE = 14;
static const uint8_t EOF_CRC_SIZE = 2;
static const uint16_t BUF_SIZE = 256;

fit_error_t
fit_parse_header(fit_t* data)
{
  fit_error_t error = FIT_ERROR_GENERAL;
  data->blob.pos = 0;

  data->header.header_size = get8bits(&data->blob);
  if (data->header.header_size != HEADER_SIZE)
    {
      FIT_LOG(FIT_LOG_ERROR, "Header size: %d, expected %d.\n",
              data->header.header_size, HEADER_SIZE);
      error = FIT_ERROR_BAD_HEADER;
      goto out;
    }
  data->header.protocol_version = get8bits(&data->blob);
  data->header.profile_version = get16bits(&data->blob, 0);
  data->header.data_size = get32bits(&data->blob, 0);
  if (data->header.data_size != (data->blob.size - HEADER_SIZE - EOF_CRC_SIZE))
    {
      FIT_LOG(FIT_LOG_ERROR, "Header data size: %u, file size: %u.\n",
              data->header.data_size, data->blob.size);
      error = FIT_ERROR_BAD_HEADER;
      goto out;
    }
  memcpy(data->header.ascii_dotfit,
         (void*)(data->blob.data + data->blob.pos), 4);
  if (strncmp(data->header.ascii_dotfit, DOTFIT, 4) != 0)
    {
      FIT_LOG(FIT_LOG_ERROR, "Expected .FIT in header.\n");
      error = FIT_ERROR_BAD_HEADER;
      goto out;
    }
  data->blob.pos += 4;
  data->header.crc = get16bits(&data->blob , 0);

  error = FIT_SUCCESS;

out:

  return error;
}


fit_error_t
fit_parse_definition_message(fit_t* data)
{
  fit_error_t error = FIT_ERROR_GENERAL;
  uint8_t i = 0;
  fit_field_definition_t** last_field = NULL;
  fit_definition_message_t** last_def = NULL;
  fit_definition_message_t* msg =
    calloc(1, sizeof(fit_definition_message_t));
  msg->fields = NULL;

  msg->local_message_type =
    data->blob.data[data->blob.pos] &
    FIT_NORMAL_HEADER_LOCAL_MESSAGE_TYPE_MASK;
  data->blob.pos += 1;

  msg->reserved = get8bits(&data->blob);
  msg->architecture = get8bits(&data->blob);
  msg->global_message_number = get16bits(&data->blob, msg->architecture);
  msg->num_fields = get8bits(&data->blob);
  msg->next = NULL;

  /*
  TODO: There probably should be some checks for the parsed data here
  */
  error = FIT_SUCCESS;

  /*
     Parse the definition fields
   */
  for (i = 0; i < msg->num_fields; ++i)
    {
      fit_field_definition_t* fdef =
        (fit_field_definition_t*)malloc(sizeof(fit_field_definition_t));
      fdef->next = NULL;
      fdef->field_definition_number = get8bits(&data->blob);
      fdef->size = get8bits(&data->blob);
      fdef->base_type = get8bits(&data->blob);

      /* Insert the field definition to the list of field definitions */
      last_field = &(msg->fields);
      while (*last_field != NULL)
        {
          last_field = (fit_field_definition_t**)&((*last_field)->next);
        }
      fdef->next = (struct fit_field_definition_t*)*last_field;
      *last_field = fdef;
    }

  /*
     Insert the definition message to the list of definition messages
   */
  last_def =
    (fit_definition_message_t**)&(data->definitions);
  while (*last_def != NULL)
    {
      last_def = (fit_definition_message_t**)&((*last_def)->next);
    }
  *last_def = msg;

  /*
  fit_log_definition_message(msg);
  */

  return error;
}

fit_error_t
fit_parse_data_message(fit_t* data, fit_message_t** message)
{
  fit_error_t ret_val = FIT_ERROR_GENERAL;
  uint8_t index = 0;
  fit_definition_message_t** message_type = &(data->definitions);
  fit_field_definition_t* def = NULL;
  fit_data_field_t* field = NULL;
  fit_data_field_t** field_iter = NULL;
  void* parsed_data = NULL;

  while (*message_type != NULL &&
         (*message_type)->local_message_type !=
         (data->blob.data[data->blob.pos] &
          FIT_NORMAL_HEADER_LOCAL_MESSAGE_TYPE_MASK))
    {
      message_type = (fit_definition_message_t**)&(*message_type)->next;
    }
  if (*message_type != NULL)
    {
      *message = calloc(1, sizeof(fit_message_t));
      (*message)->global_message_number =
        (*message_type)->global_message_number;

      ret_val = FIT_SUCCESS;
      data->blob.pos += 1;
      FIT_LOG(FIT_LOG_DEBUG2, "global message type: %d\n",
              (*message_type)->global_message_number);

      def = (*message_type)->fields;
      while (def != NULL)
        {
          void* (*parse)(uint8_t*, uint8_t*, uint8_t) = NULL;
          for (index = 0;
               index < (sizeof(FIT_BASE_TYPES) / sizeof(FIT_BASE_TYPES[0]));
               ++index )
            {
              if (def->base_type == FIT_BASE_TYPES[index].base_type_field)
                {
                  parse = FIT_BASE_TYPES[index].parse_type;
                  break;
                }
            }

          if (parse != NULL)
            {
              uint8_t size = def->size;
              parsed_data = parse(&(data->blob.data[data->blob.pos]), &size,
                    (*message_type)->architecture);
              if (parsed_data != NULL)
                {
                  field = calloc(1, sizeof(fit_data_field_t));
                  field->type = FIT_BASE_TYPES[index].type;
                  field->field_num = def->field_definition_number;
                  switch (FIT_BASE_TYPES[index].type)
                    {
                    case ENUM:
                    case S8:
                    case U8:
                    case U8Z:
                    case BYTE:
                      field->data.u8 = *((uint8_t*)parsed_data);
                      break;
                    case S16:
                    case U16:
                    case U16Z:
                      field->data.u16 = *((uint16_t*)parsed_data);
                      break;
                    case S32:
                    case U32:
                    case U32Z:
                      field->data.u32 = *((uint32_t*)parsed_data);
                      break;
                    case FLOAT32:
                      field->data.float32 = *((float*)parsed_data);
                      break;
                    case FLOAT64:
                      field->data.float64 = *((double*)parsed_data);
                      break;
                    case STRING:
                      field->data.string
                        = calloc(1, strnlen(parsed_data, BUF_SIZE));
                      strncpy(field->data.string, (char*)parsed_data,
                              BUF_SIZE);
                      break;
                    default:
                      free(field);
                      FIT_LOG(FIT_LOG_ERROR,
                              "Unkown type for data field: %d.",
                              FIT_BASE_TYPES[index].type);
                      field = NULL;
                      break;
                    }

                  if (field != NULL)
                    {
                      field_iter = &(*message)->fields;
                      while (*field_iter != NULL)
                        {
                          field_iter = (fit_data_field_t**)&(*field_iter)->next;
                        }
                      *field_iter = field;
                    }
                }
              data->blob.pos += size;
            }
          else
            {
              FIT_LOG(FIT_LOG_INFO,
                      "Could not find parser for data message.\n");
            }
          def = (fit_field_definition_t*)def->next;
        }
    }
  else
    {
      FIT_LOG(FIT_LOG_ERROR,
              "Could not find definition message for local type \"%d\"\n",
              data->blob.data[data->blob.pos] &
              FIT_NORMAL_HEADER_LOCAL_MESSAGE_TYPE_MASK);
    }
  return ret_val;
}

fit_error_t
fit_parse_compressed_timestamp_message(fit_t* data)
{
  fit_error_t ret_val = FIT_ERROR_GENERAL;
  fit_definition_message_t** message_type = &(data->definitions);
  while (*message_type != NULL &&
         (*message_type)->local_message_type !=
          (data->blob.data[data->blob.pos] &
            FIT_COMPRESSED_TIMESTAMP_HEADER_LOCAL_MESSAGE_TYPE_MASK))
    {
      message_type = (fit_definition_message_t**)&(*message_type)->next;
    }
  if (*message_type != NULL)
    {
      FIT_LOG(FIT_LOG_DEBUG, "Found message local message type: %d",
              (*message_type)->local_message_type);
      FIT_LOG(FIT_LOG_ERROR, "Compressed timetamp not implemented.");
    }
  else
    {
      FIT_LOG(FIT_LOG_ERROR,
              "Could not find definition message for local type \"%d\"\n",
              data->blob.data[data->blob.pos] &
              FIT_NORMAL_HEADER_LOCAL_MESSAGE_TYPE_MASK);
    }

  return ret_val;
}

fit_error_t
fit_parse_record(fit_t* data, fit_message_t** message)
{
  fit_error_t error = FIT_ERROR_GENERAL;

  if (data->blob.pos >= data->header.data_size)
    {
      error = FIT_DATA_END;
    }
  else if (data->blob.data[data->blob.pos] &
      FIT_COMPRESSED_TIMESTAMP_HEADER_BIT)
    {
      FIT_LOG(FIT_LOG_DEBUG, "compressed timestamp header\n");
      error = fit_parse_compressed_timestamp_message(data);
      if (error != FIT_SUCCESS)
        {
          FIT_LOG(FIT_LOG_ERROR,
                  "Error parsing compressed timestamp message record at position %u.\n",
                  data->blob.pos );
        }
    }
  else if ((data->blob.data[data->blob.pos] & FIT_NORMAL_HEADER_MASK) ==
           FIT_DEFINITION_MESSAGE_BIT)
    {
      FIT_LOG(FIT_LOG_DEBUG, "definition message\n");
      error = fit_parse_definition_message(data);
      if (error != FIT_SUCCESS)
        {
          FIT_LOG(FIT_LOG_ERROR,
                  "Error parsing definition message record at position %u.\n",
                  data->blob.pos );
        }
    }
  else if ((data->blob.data[data->blob.pos] &
            FIT_NORMAL_HEADER_MASK) == 0x00)
    {
      FIT_LOG(FIT_LOG_DEBUG2, "data message\n");
      error = fit_parse_data_message(data, message);
      if (error != FIT_SUCCESS)
        {
          FIT_LOG(FIT_LOG_ERROR,
                  "Error parsing data message record at position %u.\n",
                  data->blob.pos );
        }
    }
  else
    {
      FIT_LOG( FIT_LOG_ERROR, "Could not determine record type." );
      error = FIT_ERROR_BAD_RECORD_TYPE;
    }

  return error;
}

void
fit_log_header( fit_t* data )
{
  FIT_LOG( FIT_LOG_INFO, "header size: %d\n", data->header.header_size );
  FIT_LOG( FIT_LOG_INFO, "protocol version: %d\n",
           data->header.protocol_version );
  FIT_LOG( FIT_LOG_INFO, "profile version: %d\n",
           data->header.profile_version );
  FIT_LOG( FIT_LOG_INFO, "data size: %d\n", data->header.data_size );
  FIT_LOG( FIT_LOG_INFO, "ascii dotfit: %.4s\n", data->header.ascii_dotfit );
  FIT_LOG( FIT_LOG_INFO, "crc: %d\n", data->header.crc );
  FIT_LOG( FIT_LOG_INFO, "position in data (0 based): %u\n",
           data->blob.pos );
}

void
fit_log_definition_message( fit_definition_message_t* data )
{
  fit_field_definition_t** field = NULL;
  FIT_LOG( FIT_LOG_INFO, "local_message_type: %d\n",
           data->local_message_type );
  FIT_LOG( FIT_LOG_INFO, "reserved: %d\n", data->reserved );
  FIT_LOG( FIT_LOG_INFO, "architecture: %d\n", data->architecture );
  FIT_LOG( FIT_LOG_INFO, "global_message_number: %d\n",
           data->global_message_number );
  FIT_LOG( FIT_LOG_INFO, "num_fields: %d\n", data->num_fields );
  field = &(data->fields);
  while( *field != NULL )
    {
      FIT_LOG( FIT_LOG_INFO, "\tfield definition number: %d\n",
               (*field)->field_definition_number );
      FIT_LOG( FIT_LOG_INFO, "\tsize: %d\n", (*field)->size );
      FIT_LOG( FIT_LOG_INFO, "\tbase type: %02X\n", (*field)->base_type );
      field = (fit_field_definition_t**)&(*field)->next;
    }
}

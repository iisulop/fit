#include "fit.h"
#include "fit_record_fields.h"

#include <stdio.h>

int main(int argc, char* argv[])
{
  fit_t* fit = NULL;
  fit_error_t error;
  fit_message_t* message = NULL;
  fit_data_field_t* field = NULL;
  uint16_t i = 0;
  const char* name = NULL;

  if (argc != 2)
    {
      printf("Usage: %s FILE\n", argv[0]);
      return 1;
    }

  fit = fit_loadf(argv[1], 0, &error);

  while (error == FIT_SUCCESS)
    {
      message = fit_get_next_data_message(fit, &error);
      if (message == NULL || message->global_message_number != 20)
        {
          continue;
        }
      do
        {
          field = fit_get_next_data_field(message, field);
          if (field != NULL)
            {
              if (fit_is_integer(field))
                {
                  int64_t num = 0;
                  if (fit_get_integer_value(field, &num) == FIT_SUCCESS)
                    {
                      for (i = 0;
                           i < sizeof(FIT_RECORD_FIELDS) /
                               sizeof(FIT_RECORD_FIELDS[0]); ++i)
                        {
                          if (FIT_RECORD_FIELDS[i].num == field->field_num)
                            {
                              name = FIT_RECORD_FIELDS[i].name;
                              break;
                            }
                        }
                      printf("%s\t%lld\n", name, num);
                      /* handle all fields */
                    }
                  else
                    {
                    }
                }
            }
        } while (field != NULL);
      printf("\n");
    }

  fit_release(&fit);

  return 0;
}

#!/usr/bin/env python3

import operator
import os
import re
import sys


FILENAME = '../../fit/c/fit_example.h'


INVALID_VALUES = {
        'FIT_ENUM_INVALID': 0xff,
        'FIT_SINT_INVALID': 0x7f,
        'FIT_UINT8_INVALID': 0xff,
        'FIT_SINT16_INVALID': 0x7fff,
        'FIT_UINT16_INVALID': 0xffff,
        'FIT_SINT32_INVALID': 0x7fffffff,
        'FIT_UINT32_INVALID': 0xffffffff,
        'FIT_STRING_INVALID': 0x00,
        'FIT_FLOAT32_INVALID': 0xffffffff,
        'FIT_FLOAT64_INVALID': 0xffffffffffffffff,
        'FIT_UINT8Z_INVALID': 0x00,
        'FIT_UINT16Z_INVALID': 0x0000,
        'FIT_UINT32Z_INVALID': 0x00000000,
        'FIT_BYTE_INVALID': 0xFF
    }

PART_TYPES = 1
PART_MESSAGE_CONVERSION_STRUCTURES = 2
PART_MESSAGES = 3


class FitData():
    def __init__(self, data_type):
        self.data_type = data_type
        self.fields = {}

    #def check_line(self, line):
        #raise NotImplementedError()

    #def parse_line(self, line):
        #raise NotImplementedError()

    def write_output(self, directory):
        #if not os.path.exists(out_dir):
            #os.makedirs(out_dir)
        #filename = self.data_type
        #with open(filename, 'w') as f:
        for key in self.fields.keys():
            print("%s : %s (%s)" % (key, self.fields[key][0],
                self.fields[key][1]))
        #raise NotImplementedError()


class FitType(FitData):
    __value_re = \
            re.compile(r'\(\(.*\)(?P<num>(0x)?[0-9a-fA-F]+)\)(.*// *(?P<comment>.*$))?')
    def __init__(self, data_type, data_name):
        self.data_name = data_name
        self.ongoing = False
        regex = r'#define %s.+%s' % (data_name, data_name)
        self.regex = re.compile(regex)
        super(FitType, self).__init__(data_type)

    def check_start(self, line):
        #print(line)
        # Check if a type define starts
        if (not self.ongoing and
            "typedef" in line and
            "%s;" % self.data_name in line):
            self.ongoing = True
            return True

    def parse_line(self, line):
        # Get the type and value
        #print(line)
        name = line.split()[1]
        matches = self.__value_re.search(line)
        if matches != None and "num" in matches.groupdict():
            value = matches.group("num")
            if value.startswith("0x"):
                value = int(value, 16)
            else:
                value = int(value)
            comment = matches.group("comment")
            #print("%s = %s (%s)" %(name, value, comment))
            self.fields[name] = (value, comment)
        elif "%s_INVALID" % (self.data_name) in line:
            value = INVALID_VALUES[line.split()[2]]
            comment = None
            #print("%s = %s (%s)" %(name, value, comment))
        else:
            # Check if the type defines end
            if ("#define" in line and
                ("%s_COUNT" % (self.data_name)) in line):
                self.ongoing = False
                return False

        return True

    def write_output(self, directory):
        out_dir = os.path.join(directory, "types")
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        filename = os.path.join(out_dir, self.data_name.lower() + ".h")
        with open(filename, 'w') as f:
            f.write("typedef enum {\n")

            keys = [t[0] for t in sorted(self.fields.items(), key=operator.itemgetter(1))]
            #sorted(list(self.fields.keys()))

            for key in keys[0:-1]:
                if self.fields[key][1] != None:
                    f.write("  %s = %s, // %s,\n" % (key, self.fields[key][0],
                                                 self.fields[key][1]))
                else:
                    f.write("  %s = %s\n" % (key, self.fields[key][0]))

            if self.fields[list(self.fields.keys())[-1]][1] != None:
                f.write("  %s = %s, // %s\n" % (keys[-1],
                                            self.fields[keys[-1]][0],
                                            self.fields[keys[-1]][1]))
            else:
                f.write("  %s = %s\n" % (keys[-1],
                                     self.fields[keys[-1]][0]))
            f.write("} %s;\n" % (self.data_name))

        with open(os.path.join(directory, "types.h"), 'a') as f:
            f.write("#include \"%s.h\"\n" %(self.data_name.lower()))


class FitMessageFields(FitData):
    __message_field_re = \
            re.compile(r'(?P<name>FIT_\w+) = (?P<num>[0-9]+)')
    def __init__(self, message_definition):
        message_definition.pop(0) # The first line is just '{'
        data_type = message_definition.pop()[2:-2]
        super(FitMessageFields, self).__init__(data_type)

        for line in message_definition:
            matches = self.__message_field_re.search(line)
            value = matches.group("num")
            if value.startswith("0x"):
                value = int(value, 16)
            else:
                value = int(value)
            self.fields[matches.group("name")] = value


    def write_output(self, directory):
        out_dir = os.path.join(directory, "messages")
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        filename = os.path.join(out_dir, self.data_type.lower() + ".h")
        with open(filename, 'w') as f:
            fields_sorted = sorted(self.fields.items(), key=lambda x: int(x[1]))
            f.write("typedef enum {\n")
            first = True
            for field in fields_sorted:
                if not first :
                    f.write(",\n")
                else:
                    first = False
                f.write("  %s = %s" % (field[0], field[1]))
            f.write("\n} %s;\n" % (self.data_type))

        with open(os.path.join(directory, "messages.h"), 'a') as f:
            f.write("#include \"%s.h\"\n" %(self.data_type.lower()))


#message_number_re = re.compile(r'^#define FIT_MESG_NUM.+FIT_MESG_NUM')

types = []
messages = []
# Find the defined types
type_re = re.compile(r'typedef FIT_(?P<type>\w+) (?P<name>\w+);')

with open(FILENAME) as f:
    for line in f:
        typedef = type_re.search(line)
        if (typedef != None):
            type_to_add = None

            fit_type = typedef.group('type')
            fit_name = typedef.group('name')
            type_to_add = FitType(fit_type, fit_name)
            #print("Adding %s : %s" % (fit_name, fit_type))
            types.append(type_to_add)


ongoing_type = None
message_definition = None

parse_state = PART_TYPES

with open(FILENAME) as f:
    for line in f:
        # The messages part starts here
        if "// Messages" in line:
            parse_state = PART_MESSAGES
            continue

        # Skip empty lines and comment lines
        if (len(line) == 0 or
            line[0:2] == "//"):
            continue

        # Parse types
        if parse_state == PART_TYPES:
            if ongoing_type != None:
                if ongoing_type.parse_line(line):
                    continue

            found_start = False
            for t in types:
                if t.check_start(line):
                    ongoing_type = t
                    found_start = True
                    break

            if not found_start:
                ongoing_type = None
        # Parse messages
        elif parse_state == PART_MESSAGES:
            if line.strip() == "typedef enum":
                message_definition = []
                continue
            if message_definition != None:
                message_definition.append(line)
                if ("} FIT_" in line and
                    "_NUM" in line):
                    message = FitMessageFields(message_definition)
                    messages.append(message)
                    message_definition = None


types = sorted(types, key=lambda x: x.data_name)
messages = sorted(messages, key=lambda x: x.data_type)

for t in types:
    t.write_output("inc")

for m in messages:
    m.write_output("inc")

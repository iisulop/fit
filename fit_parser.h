#ifndef FIT_PARSER_H
#define FIT_PARSER_H

#include "fit.h"


fit_error_t
fit_parse_header(fit_t* data);

fit_error_t
fit_parse_data_message(fit_t* fit, fit_message_t** message);

fit_error_t
fit_parse_definition_message(fit_t* data);

fit_error_t
fit_parse_compressed_timestamp_message(fit_t* data);

fit_error_t
fit_parse_record(fit_t* data, fit_message_t** message);

void
fit_log_header(fit_t* data);

void
fit_log_definition_message(fit_definition_message_t* data);


#endif /* FIT_PARSER_H */

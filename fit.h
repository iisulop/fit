#ifndef FIT_H
#define FIT_H

#include <stdlib.h>

#include "fit_types.h"


typedef enum fit_error_t {
    FIT_SUCCESS = 0,

    FIT_ERROR_GENERAL,
    FIT_ERROR_OUT_OF_MEMORY,
    FIT_ERROR_DATA_NULL,

    FIT_ERROR_CANNOT_OPEN_FILE,
    FIT_ERROR_CANNOT_GET_FILE_LENGTH,
    FIT_ERROR_CANNOT_READ_FILE,

    FIT_ERROR_BAD_HEADER,
    FIT_ERROR_BAD_RECORD_TYPE,

    FIT_ERROR_NOT_ENUM,
    FIT_ERROR_NOT_NUMBER,
    FIT_ERROR_NOT_SIGNED,
    FIT_ERROR_NOT_UNSIGNED,
    FIT_ERROR_NOT_REAL,
    FIT_ERROR_NOT_STRING,

    FIT_DATA_END
} fit_error_t;

fit_t*
fit_loadb(const uint8_t* buf, const size_t len, const size_t flags, fit_error_t* error);

void
fit_release(fit_t** d);

#ifndef NO_FILE_SUPPORT
fit_t*
fit_loadf(const char* filename, const size_t flags, fit_error_t* error);
#endif /* NO_FILE_SUPPORT */

fit_message_t*
fit_get_next_data_message(fit_t* fit, fit_error_t* error);

fit_data_field_t*
fit_get_next_data_field(fit_message_t* message, fit_data_field_t* field);

uint8_t
fit_is_enum(fit_data_field_t* field);

uint8_t
fit_is_integer(fit_data_field_t* field);

uint8_t
fit_is_signed(fit_data_field_t* field);

uint8_t
fit_is_unsigned(fit_data_field_t* field);

uint8_t
fit_is_real(fit_data_field_t* field);

uint8_t
fit_is_string(fit_data_field_t* field);

fit_error_t
fit_get_enum_value(fit_data_field_t* field, uint8_t* value);

fit_error_t
fit_get_integer_value(fit_data_field_t* field, int64_t* value);

fit_error_t
fit_get_signed_value(fit_data_field_t* field, int64_t* value);

fit_error_t
fit_get_unsigned_value(fit_data_field_t* field, uint64_t* value);

fit_error_t
fit_get_real_value(fit_data_field_t* field, double* value);

fit_error_t
fit_get_string_value(fit_data_field_t* field, char** value);

void
fit_release_message(fit_message_t* message);

#endif /* FIT_H */
